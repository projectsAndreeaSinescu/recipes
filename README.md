# Recipes

## Ce este Recipes?
Este o aplicatie web, dezvoltata cu Java si framework-ul Spring, in care se pot vizualiza retete adaugate de utilizatorii logati, se pot
face liste de cumparaturi in care se pot gestiona ingredientele
cumparate pentru retetele dorite si se pot adauga retete la
favorite.

## Pagina principala
In pagina principala se pot vizualiza toate retetele tuturor
utilizatorilor. De asemenea, exista si posibilitatea de a 
vizualiza doar retetele bauturilor sau cele ale mancarurilor, 
dar si cele pe care le-a creat utilizatorul logat curent.
![](./docs/pagina-principala.PNG)

## Lista de cumparaturi
In lista de cumparaturi se pot bifa ingredientele pe care
utilizatorul deja le-a cumparat pentru a vedea mai apoi de ce
anume mai are nevoie.
![](./docs/lista-de-cumparaturi.PNG)

De asemenea, utilizatorul dispune si de o pagina creata
pentru a bifa toate ingredientele cu acelasi nume pe care 
utilizatorul le detine, astfel fiindu-i mai usor si rapid
sa gestioneze lucrurile de care mai are nevoie.
![](./docs/lista-de-cumparaturi-ingrediente.PNG)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- Java
- docker
- docker-compose

### Start the application
- Start the database:
```
docker-compose up -d
```
- Open `src/main/java/com/fmi/recipes/RecipesApplication` and press the `Run` button.

### Backup and restore the database during development

#### Backup
```
docker exec recipes_postgres_1 pg_dump -U recipes recipes > backup.sql
```

#### Restore
1. Stop the containers
```
docker-compose down
```

2. Delete the postgres volume
```
docker volume rm recipes-postgres-data
```

3. Start the postgres container and restore the data
```
docker-compose up -d
cat backup.sql | docker exec -i recipes_postgres_1 psql -U recipes recipes
```

## Deployment
```
docker-compose -f docker-compose.yml -f docker-compose.qa.yml up -d --build
```

## Built With
- [Spring Framework](https://start.spring.io/)
- [Thymeleaf](https://www.thymeleaf.org/)
- [PostgreSQL](https://www.postgresql.org/)

## Inspired from
- https://hellokoding.com/registration-and-login-example-with-spring-security-spring-boot-spring-data-jpa-hsql-jsp/
- https://github.com/hellokoding/hellokoding-courses

- https://www.mkyong.com/spring-boot/spring-boot-spring-security-thymeleaf-example/
 
- https://www.codebyamir.com/blog/user-account-registration-with-spring-boot
- https://github.com/codebyamir-zz/spring-boot-user-account-registration
 
- https://getbootstrap.com/docs/3.4/examples/sticky-footer-navbar/
- https://www.w3schools.com/bootstrap/bootstrap_navbar.asp

- https://www.javaguides.net/2019/04/spring-boot-thymeleaf-crud-example-tutorial.html
- https://github.com/springframeworkguru/spring5-recipe-app/tree/display-image-from-db
