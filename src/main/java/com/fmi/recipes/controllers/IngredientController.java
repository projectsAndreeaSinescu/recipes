package com.fmi.recipes.controllers;

import com.fmi.recipes.model.Ingredient;
import com.fmi.recipes.model.Recipe;
import com.fmi.recipes.repository.IngredientRepository;
import com.fmi.recipes.repository.RecipeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class IngredientController {
    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;

    public IngredientController(RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    @PostMapping("recipes/{id}/add-ingredient")
    public String addIngredient(
        @PathVariable("id") long id,
        @Valid Ingredient ingredient,
        BindingResult result,
        Model model,
        Authentication authentication,
        RedirectAttributes attributes
    ) {
        Recipe recipe = recipeRepository.findById(id);

        if (recipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        if (!recipe.getAuthor().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to view this page.");
        }

        if (result.hasFieldErrors("name") || result.hasFieldErrors("quantity")) {
            model.addAttribute("recipe", recipe);
            return "recipes-form";
        }
        ingredient.setRecipe(recipe);
        ingredientRepository.save(ingredient);
        attributes.addFlashAttribute("message", "The ingredient has been saved.");
        return "redirect:/recipes/" + id + "/edit";
    }

    @PostMapping("ingredients/{id}/delete")
    public String deleteIngredient(@PathVariable("id") long id, Authentication authentication, RedirectAttributes attributes) {
        Ingredient ingredient = ingredientRepository.findById(id);
        if (ingredient == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find ingredient.");
        }

        if (!ingredient.getRecipe().getAuthor().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to perform this action.");
        }

        ingredientRepository.delete(ingredient);
        attributes.addFlashAttribute("message", "The ingredient has been deleted.");
        return "redirect:/recipes/" + ingredient.getRecipe().getId() + "/edit";
    }
}
