package com.fmi.recipes.controllers;

import com.fmi.recipes.model.*;
import com.fmi.recipes.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
@RequestMapping("/favorites/")
public class FavoritesController {
    private final RecipeRepository recipeRepository;
    private final UserRepository userRepository;

    public FavoritesController(RecipeRepository recipeRepository, IngredientRepository ingredientRepository,
                               UserRepository userRepository, ShoppingCartRecipeRepository shoppingCartRecipeRepository,
                               ShoppingCartIngredientRepository shoppingCartIngredientRepository) {
        this.recipeRepository = recipeRepository;
        this.userRepository = userRepository;
    }

    @PostMapping("{id}/add-to-favorites/{fromFavorites}/{fromView}")
    public String addToCart(@PathVariable("id") long id, @PathVariable("fromFavorites") boolean fromFavorites,
                            @PathVariable("fromView") boolean fromView, Authentication authentication,
                            RedirectAttributes attributes) {
        User currentUser = userRepository.findByUsername(authentication.getName());

        Recipe recipe = recipeRepository.findById(id);

        if (recipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        Set<User> users = recipe.getFavorites();

        for(User user : users) {
            if(user.getUsername().equals(currentUser.getUsername())) {
                users.remove(user);
                recipe.setFavorites(users);
                recipeRepository.save(recipe);
                attributes.addFlashAttribute("message", "You successfully removed the recipe " +
                        recipe.getName() + " from the favorites list.");
                if (fromFavorites) {
                    return "redirect:/favorites/favorites-list";
                }
                else if (fromView) {
                    return "redirect:/recipes/" + id + "/view";
                }
                else {
                    return "redirect:/";
                }
            }
        }

        users.add(currentUser);
        recipe.setFavorites(users);

        recipeRepository.save(recipe);

        attributes.addFlashAttribute("message", "You successfully added the recipe " +
                recipe.getName() + " in the favorites list.");
        if (fromView) {
            return "redirect:/recipes/" + id + "/view";
        }
        else {
            return "redirect:/favorites/favorites-list";
        }
    }

    @GetMapping("favorites-list")
    public String myRecipes(Model model, Authentication authentication) {
        User currentUser = userRepository.findByUsername(authentication.getName());

        Iterable<Recipe> recipes = currentUser.getFavoriteRecipes();

        List<Boolean> favorites = new ArrayList<Boolean>(Arrays.asList(new Boolean[((Collection<Recipe>)recipes).size()]));
        Collections.fill(favorites, Boolean.TRUE);

        model.addAttribute("fromFavorites", true);
        model.addAttribute("favorites", favorites);
        model.addAttribute("recipes", recipes);
        model.addAttribute("title", "Favorites list");
        model.addAttribute("hideUser", true);
        return "recipes-list";
    }
}
