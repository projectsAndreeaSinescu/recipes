package com.fmi.recipes.controllers;

import com.fmi.recipes.model.*;
import com.fmi.recipes.repository.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Controller
@RequestMapping("/cart/")
public class CartController {
    private final RecipeRepository recipeRepository;
    private final UserRepository userRepository;
    private final ShoppingCartIngredientRepository shoppingCartIngredientRepository;
    private final ShoppingCartRecipeRepository shoppingCartRecipeRepository;

    public CartController(RecipeRepository recipeRepository, IngredientRepository ingredientRepository,
                          UserRepository userRepository, ShoppingCartRecipeRepository shoppingCartRecipeRepository,
                          ShoppingCartIngredientRepository shoppingCartIngredientRepository) {
        this.recipeRepository = recipeRepository;
        this.userRepository = userRepository;
        this.shoppingCartIngredientRepository = shoppingCartIngredientRepository;
        this.shoppingCartRecipeRepository = shoppingCartRecipeRepository;
    }

    private static ShoppingCartRecipe sortIngredients(ShoppingCartRecipe shoppingCartRecipe) {
        Recipe recipe = shoppingCartRecipe.getRecipe();
        if (recipe != null) {
            Set<ShoppingCartIngredient> shoppingCartIngredients = shoppingCartRecipe.getIngredients();
            Set<Ingredient> ingredients = recipe.getIngredients();
            if (shoppingCartIngredients.size() == ingredients.size()) {
                Set<Pair<String, String>> shoppingCartRecipeIngredientList = new HashSet<>();
                Set<Pair<String, String>> recipeIngredientList = new HashSet<>();
                for (ShoppingCartIngredient shoppingCartIngredient : shoppingCartRecipe.getIngredients()) {
                    Pair<String, String> shoppingCartRecipeIngredient = Pair.of(shoppingCartIngredient.getName(), shoppingCartIngredient.getQuantity());
                    shoppingCartRecipeIngredientList.add(shoppingCartRecipeIngredient);
                }
                for (Ingredient ingredient : recipe.getIngredients()) {
                    Pair<String, String> recipeIngredient = Pair.of(ingredient.getName(), ingredient.getQuantity());
                    recipeIngredientList.add(recipeIngredient);
                }

                if (shoppingCartRecipeIngredientList.equals(recipeIngredientList)) {
                    shoppingCartRecipe.setName(recipe.getName());
                    shoppingCartRecipe.setImage(recipe.getRawImage());
                    shoppingCartRecipe.setDrink(recipe.getIsDrink());
                    shoppingCartRecipe.setDescription(recipe.getDescription());
                }
            }
        }
        List<ShoppingCartIngredient> shoppingCartIngredientList = new ArrayList<>(shoppingCartRecipe.getIngredients());
        Collections.sort(shoppingCartIngredientList);
        shoppingCartRecipe.setIngredients(new LinkedHashSet<>(shoppingCartIngredientList));
        return shoppingCartRecipe;
    }

    @GetMapping("shopping-list")
    public String myRecipes(Model model, Authentication authentication) {
        User user = userRepository.findByUsername(authentication.getName());
        List<ShoppingCartRecipe> shoppingCartRecipeList = shoppingCartRecipeRepository.findByUser(user);
        for (int i = 0; i < shoppingCartRecipeList.size(); i ++) {
            ShoppingCartRecipe shoppingCartRecipe = shoppingCartRecipeList.get(i);

            shoppingCartRecipeList.set(i, CartController.sortIngredients(shoppingCartRecipe));
        }
        model.addAttribute("shoppingCartRecipes", shoppingCartRecipeList);
        model.addAttribute("hideUser", true);
        return "shopping-cart-list";
    }

    @GetMapping("shoppingCartRecipes/{id}/view")
    public String viewRecipe(@PathVariable("id") long id, Model model, Authentication authentication) {
        ShoppingCartRecipe shoppingCartRecipe = shoppingCartRecipeRepository.findById(id);

        if (shoppingCartRecipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        if (!shoppingCartRecipe.getUser().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to view this page.");
        }

        model.addAttribute("shoppingCartRecipe", CartController.sortIngredients(shoppingCartRecipe));

        return "shopping-cart-recipes-view";
    }

    @GetMapping("group-ingredient-list")
    public String viewRecipe(Model model, Authentication authentication) {
        User currentUser = userRepository.findByUsername(authentication.getName());
        List<ShoppingCartRecipe> shoppingCartRecipes = shoppingCartRecipeRepository.findByUser(currentUser);

        List<ShoppingCartIngredient> shoppingCartIngredientsChecked = new ArrayList<>();
        List<ShoppingCartIngredient> shoppingCartIngredientsUnchecked = new ArrayList<>();

        for (ShoppingCartRecipe shoppingCartRecipe : shoppingCartRecipes) {
            Set<ShoppingCartIngredient> shoppingCartIngredients = shoppingCartRecipe.getIngredients();
            for (ShoppingCartIngredient shoppingCartIngredient : shoppingCartIngredients) {
                if (shoppingCartIngredient.isBought()) {
                    shoppingCartIngredientsChecked.add(shoppingCartIngredient);
                }
                else {
                    shoppingCartIngredientsUnchecked.add(shoppingCartIngredient);
                }
            }
        }
        Collections.sort(shoppingCartIngredientsChecked);
        Collections.sort(shoppingCartIngredientsUnchecked);

        List<Pair<String, List<String>>> shoppingCartCheckedIngredientsPairList = new ArrayList<>();
        List<Pair<String, List<String>>> shoppingCartUncheckedIngredientsPairList = new ArrayList<>();

        String currentName = "";
        List<String> currentIngredientsQuantity = new ArrayList<>();
        for (ShoppingCartIngredient shoppingCartIngredient : shoppingCartIngredientsChecked) {
            if (currentName.equals("")) {
                currentName = shoppingCartIngredient.getName();
                currentIngredientsQuantity.add(shoppingCartIngredient.getQuantity());
            }
            else if (currentName.equals(shoppingCartIngredient.getName())){
                currentIngredientsQuantity.add(shoppingCartIngredient.getQuantity());
            }
            else {
                Collections.sort(currentIngredientsQuantity);
                Pair<String, List<String>> currentIngredientPair = Pair.of(currentName, currentIngredientsQuantity);
                shoppingCartCheckedIngredientsPairList.add(currentIngredientPair);
                currentIngredientsQuantity = new ArrayList<>();
                currentName = shoppingCartIngredient.getName();
                currentIngredientsQuantity.add(shoppingCartIngredient.getQuantity());
            }
        }

        if (currentIngredientsQuantity.size() != 0) {
            Collections.sort(currentIngredientsQuantity);
            Pair<String, List<String>> currentIngredientPair = Pair.of(currentName, currentIngredientsQuantity);
            shoppingCartCheckedIngredientsPairList.add(currentIngredientPair);
        }

        currentName = "";
        currentIngredientsQuantity = new ArrayList<>();

        for (ShoppingCartIngredient shoppingCartIngredient : shoppingCartIngredientsUnchecked) {
            if (currentName.equals("")) {
                currentName = shoppingCartIngredient.getName();
                currentIngredientsQuantity.add(shoppingCartIngredient.getQuantity());
            }
            else if (currentName.equals(shoppingCartIngredient.getName())){
                currentIngredientsQuantity.add(shoppingCartIngredient.getQuantity());
            }
            else {
                Collections.sort(currentIngredientsQuantity);
                Pair<String, List<String>> currentIngredientPair = Pair.of(currentName, currentIngredientsQuantity);
                shoppingCartUncheckedIngredientsPairList.add(currentIngredientPair);
                currentIngredientsQuantity = new ArrayList<>();
                currentName = shoppingCartIngredient.getName();
                currentIngredientsQuantity.add(shoppingCartIngredient.getQuantity());
            }
        }

        if (currentIngredientsQuantity.size() != 0) {
            Collections.sort(currentIngredientsQuantity);
            Pair<String, List<String>> currentIngredientPair = Pair.of(currentName, currentIngredientsQuantity);
            shoppingCartUncheckedIngredientsPairList.add(currentIngredientPair);
        }
        model.addAttribute("shoppingCartIngredientsChecked", shoppingCartCheckedIngredientsPairList);
        model.addAttribute("shoppingCartIngredientsUnchecked", shoppingCartUncheckedIngredientsPairList);

        return "group-ingredient-recipes-list";
    }

    @PostMapping("check-all-ingredients-by-name/{check}")
    public String checkAllIngredients(@RequestParam(value = "shoppingCartIngredientName") String shoppingCartIngredientName,
                                      @PathVariable("check") boolean check, Authentication authentication,
                                      RedirectAttributes attributes) {
        User currentUser = userRepository.findByUsername(authentication.getName());
        List<ShoppingCartIngredient> shoppingCartIngredients = shoppingCartIngredientRepository.
                findByNameAndBought(shoppingCartIngredientName, !check);
        for (ShoppingCartIngredient shoppingCartIngredient : shoppingCartIngredients) {
            if (shoppingCartIngredient.getShoppingCartRecipe().getUser().getUsername().equals(currentUser.getUsername())) {
                shoppingCartIngredient.setBought(check);
                shoppingCartIngredientRepository.save(shoppingCartIngredient);
            }
        }

        return "redirect:/cart/group-ingredient-list";
    }

    @PostMapping("{id}/check-ingredient/{idIngredient}/{fromView}")
    public String checkIngredient(@PathVariable("id") long id, @PathVariable("idIngredient") long idIngredient,
                                  @PathVariable("fromView") boolean fromView,
                                  Authentication authentication, RedirectAttributes attributes) {
        ShoppingCartRecipe shoppingCartRecipe = shoppingCartRecipeRepository.findById(id);
        ShoppingCartIngredient shoppingCartIngredient = shoppingCartIngredientRepository.findById(idIngredient);


        if (shoppingCartRecipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        if (!shoppingCartRecipe.getUser().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to view this page.");
        }

        if (shoppingCartIngredient == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find ingredient.");
        }

        shoppingCartIngredient.setBought(!(shoppingCartIngredient.isBought()));

        shoppingCartIngredientRepository.save(shoppingCartIngredient);

        String checked = shoppingCartIngredient.isBought() ? "checked" : "unchecked";
        attributes.addFlashAttribute("message", "You successfully " + checked + " the ingredient " + shoppingCartIngredient.getName() + " for " +
                shoppingCartRecipe.getName() + " in the shopping list.");

        if (fromView) {
            return "redirect:/cart/shoppingCartRecipes/" + shoppingCartRecipe.getId() + "/view";
        }
        else {
            return "redirect:/cart/shopping-list";
        }
    }

    @PostMapping("{id}/add-to-cart/{fromView}")
    public String addToCart(@PathVariable("id") long id, @PathVariable("fromView") boolean fromView,
                            Authentication authentication, RedirectAttributes attributes) {
        Recipe recipe = recipeRepository.findById(id);

        if (recipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        List<ShoppingCartRecipe> listOfShoppingCartRecipe = shoppingCartRecipeRepository.findByRecipe(recipe);

        for(ShoppingCartRecipe shoppingCartRecipe : listOfShoppingCartRecipe) {
            if(shoppingCartRecipe.getUser().getUsername().equals(authentication.getName())) {
                attributes.addFlashAttribute("error", "You already have this item in the cart.");
                if (fromView) {
                    return "redirect:/recipes/" + id + "/view";
                }
                else {
                    return "redirect:/";
                }
            }
        }


        ShoppingCartRecipe shoppingCartRecipe = new ShoppingCartRecipe();
        shoppingCartRecipe.setDescription(recipe.getDescription());
        shoppingCartRecipe.setName(recipe.getName());
        shoppingCartRecipe.setRecipe(recipe);
        shoppingCartRecipe.setDrink(recipe.getIsDrink());
        shoppingCartRecipe.setImage(recipe.getRawImage());
        User currentUser = userRepository.findByUsername(authentication.getName());
        shoppingCartRecipe.setUser(currentUser);
        shoppingCartRecipe = shoppingCartRecipeRepository.save(shoppingCartRecipe);

        for (Ingredient ingredient : recipe.getIngredients()) {
            ShoppingCartIngredient shoppingCartIngredient = new ShoppingCartIngredient();
            shoppingCartIngredient.setName(ingredient.getName());
            shoppingCartIngredient.setQuantity(ingredient.getQuantity());
            shoppingCartIngredient.setBought(false);
            shoppingCartIngredient.setShoppingCartRecipe(shoppingCartRecipe);
            shoppingCartIngredientRepository.save(shoppingCartIngredient);
        }

        attributes.addFlashAttribute("message", "You successfully added the ingredients for " +
                recipe.getName() + " in the shopping list.");

        return "redirect:/cart/shopping-list";
    }

    @GetMapping("cart-recipes/{id}/image")
    public void renderImageFromDB(@PathVariable("id") long id, HttpServletResponse response) throws IOException {
        ShoppingCartRecipe shoppingCartRecipe = shoppingCartRecipeRepository.findById(id);

        if (shoppingCartRecipe == null || shoppingCartRecipe.getImage() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find shoppingCartRecipe image.");
        }

        response.setContentType("image/jpeg");
        InputStream is = new ByteArrayInputStream(shoppingCartRecipe.getImage());
        IOUtils.copy(is, response.getOutputStream());
    }

    @PostMapping("clear-shopping-cart")
    public String deleteRecipe(Authentication authentication, RedirectAttributes attributes) {
        User user = userRepository.findByUsername(authentication.getName());
        List<ShoppingCartRecipe>  shoppingCartRecipeList = shoppingCartRecipeRepository.findByUser(user);

        if (shoppingCartRecipeList == null || shoppingCartRecipeList.isEmpty()) {
            attributes.addFlashAttribute("message", "The shopping list is already empty.");
        }
        else {
            for (ShoppingCartRecipe shoppingCartRecipe : shoppingCartRecipeList) {
                shoppingCartRecipeRepository.delete(shoppingCartRecipe);
            }
            attributes.addFlashAttribute("message", "The shopping list has been deleted.");
        }
        return "redirect:/cart/shopping-list";

    }
}
