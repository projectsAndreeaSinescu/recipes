package com.fmi.recipes.controllers;

import com.fmi.recipes.model.Ingredient;
import com.fmi.recipes.model.Recipe;
import com.fmi.recipes.model.User;
import com.fmi.recipes.repository.RecipeRepository;
import com.fmi.recipes.repository.UserRepository;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class RecipesController {
    private final RecipeRepository recipeRepository;
    private final UserRepository userRepository;

    public RecipesController(RecipeRepository recipeRepository, UserRepository userRepository) {
        this.recipeRepository = recipeRepository;
        this.userRepository = userRepository;
    }

    public static List<Boolean> favoritesList(Iterable<Recipe> recipes) {
        List<Boolean> favorites = new ArrayList<>();
        for (Recipe recipe : recipes) {
            boolean favorite = false;
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            if (username != null) {
                for (User user : recipe.getFavorites()) {
                    if (user.getUsername().equals(username)) {
                        favorite = true;
                        break;
                    }
                }
            }
            favorites.add(favorite);
        }
        return favorites;
    }

    @GetMapping({"", "recipes"})
    public String allRecipes(Model model) {
        Iterable<Recipe> recipes = recipeRepository.findAll();

        List<Boolean> favorites = RecipesController.favoritesList(recipes);

        model.addAttribute("fromFavorites", false);
        model.addAttribute("favorites", favorites);
        model.addAttribute("recipes", recipes);
        model.addAttribute("title", "Recipes");
        return "recipes-list";
    }

    @GetMapping("recipes/mine")
    public String myRecipes(Model model, Authentication authentication) {
        User currentUser = userRepository.findByUsername(authentication.getName());
        Iterable<Recipe> recipes = recipeRepository.findByAuthor(currentUser);

        List<Boolean> favorites = RecipesController.favoritesList(recipes);

        model.addAttribute("fromFavorites", false);
        model.addAttribute("favorites", favorites);
        model.addAttribute("recipes", recipes);
        model.addAttribute("title", "My recipes");
        model.addAttribute("hideAuthor", true);
        return "recipes-list";
    }

    @GetMapping("drinks")
    public String drinks(Model model) {
        Iterable<Recipe> recipes = recipeRepository.findByIsDrink(true);

        List<Boolean> favorites = RecipesController.favoritesList(recipes);

        model.addAttribute("fromFavorites", false);
        model.addAttribute("favorites", favorites);
        model.addAttribute("recipes", recipes);
        model.addAttribute("title", "Drinks");
        return "recipes-list";
    }

    @GetMapping("food")
    public String food(Model model) {
        Iterable<Recipe> recipes = recipeRepository.findByIsDrink(false);

        List<Boolean> favorites = RecipesController.favoritesList(recipes);

        model.addAttribute("fromFavorites", false);
        model.addAttribute("favorites", favorites);
        model.addAttribute("recipes", recipes);
        model.addAttribute("title", "Food");
        return "recipes-list";
    }

    @GetMapping("add-recipe")
    public String showAddRecipeForm(Recipe recipe) {
        return "recipes-form";
    }

    @GetMapping("recipes/{id}/view")
    public String viewRecipe(@PathVariable("id") long id, Model model) {
        Recipe recipe = recipeRepository.findById(id);

        if (recipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (username != null) {
            boolean favorite = false;

            for (User user : recipe.getFavorites()) {
                if (user.getUsername().equals(username)) {
                    favorite = true;
                    break;
                }
            }

            model.addAttribute("favorite", favorite);
        }
        model.addAttribute("fromFavorites", false);
        model.addAttribute("recipe", recipe);
        return "recipes-view";
    }

    @PostMapping("add-recipe")
    public String addRecipe(
        @Valid Recipe recipe,
        BindingResult result,
        Model model,
        @RequestParam("recipe-image") MultipartFile file,
        Authentication authentication,
        RedirectAttributes attributes
    ) {
        if (result.hasErrors()) {
            return "recipes-form";
        }

        recipe.setImage(file);
        recipe.setAuthor(userRepository.findByUsername(authentication.getName()));
        recipeRepository.save(recipe);
        attributes.addFlashAttribute("message", "The recipe has been created.");
        return "redirect:/recipes/" + recipe.getId() + "/edit";
    }

    @GetMapping("recipes/{id}/edit")
    public String showEditRecipeForm(@PathVariable("id") long id, Model model, Authentication authentication, Ingredient ingredient) {
        Recipe recipe = recipeRepository.findById(id);

        if (recipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }
        if (!recipe.getAuthor().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to view this page.");
        }

        model.addAttribute("recipe", recipe);

        return "recipes-form";
    }

    @PostMapping("recipes/{id}/edit")
    public String editRecipe(
        @PathVariable("id") long id,
        @Valid Recipe recipe,
        BindingResult result,
        Model model,
        @RequestParam("recipe-image") MultipartFile file,
        Authentication authentication,
        RedirectAttributes attributes
    ) {
        Recipe originalRecipe = recipeRepository.findById(id);
        if (originalRecipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        if (!originalRecipe.getAuthor().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to perform this action.");
        }

        if (result.hasErrors()) {
            return "recipes-form";
        }

        // TODO Find a better way to update the recipe
        originalRecipe.setName(recipe.getName());
        originalRecipe.setDescription(recipe.getDescription());
        originalRecipe.setIsDrink(recipe.getIsDrink());
        if (!file.isEmpty()) {
            originalRecipe.setImage(file);
        }

        recipeRepository.save(originalRecipe);
        attributes.addFlashAttribute("message", "The recipe has been updated.");
        return "redirect:/recipes/" + id + "/edit";
    }

    @PostMapping("recipes/{id}/delete")
    public String deleteRecipe(@PathVariable("id") long id, Authentication authentication, RedirectAttributes attributes) {
        Recipe recipe = recipeRepository.findById(id);
        if (recipe == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe.");
        }

        if (!recipe.getAuthor().getUsername().equals(authentication.getName())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to perform this action.");
        }

        recipeRepository.delete(recipe);
        attributes.addFlashAttribute("message", "The recipe has been deleted.");
        return "redirect:/";
    }

    @GetMapping("recipes/{id}/image")
    public void renderImageFromDB(@PathVariable("id") long id, HttpServletResponse response) throws IOException {
        Recipe recipe = recipeRepository.findById(id);

        if (recipe == null || recipe.getImage() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find recipe image.");
        }

        response.setContentType("image/jpeg");
        InputStream is = new ByteArrayInputStream(recipe.getImage());
        IOUtils.copy(is, response.getOutputStream());
    }
}
