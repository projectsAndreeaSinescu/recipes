package com.fmi.recipes.service;

import com.fmi.recipes.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
