package com.fmi.recipes.repository;

import com.fmi.recipes.model.Recipe;
import com.fmi.recipes.model.ShoppingCartRecipe;
import com.fmi.recipes.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ShoppingCartRecipeRepository extends CrudRepository<ShoppingCartRecipe, Long> {
    ShoppingCartRecipe findById(long id);

    @Transactional
    List<ShoppingCartRecipe> findByRecipe(Recipe recipe);

    @Transactional
    List<ShoppingCartRecipe> findByUser(User user);
}
