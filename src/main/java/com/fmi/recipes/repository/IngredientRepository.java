package com.fmi.recipes.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.fmi.recipes.model.Ingredient;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
    List<Ingredient> findByName(String lastName);

    Ingredient findById(long id);
}
