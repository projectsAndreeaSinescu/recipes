package com.fmi.recipes.repository;

import com.fmi.recipes.model.Recipe;
import com.fmi.recipes.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Long> {
    Recipe findById(long id);

    @Transactional
    List<Recipe> findByIsDrink(boolean isDrink);

    @Transactional
    List<Recipe> findByAuthor(User author);
}
