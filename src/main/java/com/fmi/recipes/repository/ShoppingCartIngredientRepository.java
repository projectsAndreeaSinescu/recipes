package com.fmi.recipes.repository;

import com.fmi.recipes.model.ShoppingCartIngredient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ShoppingCartIngredientRepository extends CrudRepository<ShoppingCartIngredient, Long> {
    ShoppingCartIngredient findById(long id);

    @Transactional
    List<ShoppingCartIngredient> findByNameAndBought(String name, boolean isBought);
}
