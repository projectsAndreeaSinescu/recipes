package com.fmi.recipes.repository;

import com.fmi.recipes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Transactional
    User findByUsername(String username);
}
