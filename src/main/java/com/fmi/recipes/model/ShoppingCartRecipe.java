package com.fmi.recipes.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ShoppingCartRecipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private boolean isDrink;

    @NotNull
    @Size(max = 100)
    private String name;

    @NotNull
    @Column(length = 4096)
    private String description;

    @Lob
    private Byte[] image;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "shoppingCartRecipe")
    private Set<ShoppingCartIngredient> ingredients = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDrink() {
        return isDrink;
    }

    public void setDrink(boolean drink) {
        isDrink = drink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Set<ShoppingCartIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<ShoppingCartIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    public byte[] getImage() {
        byte[] byteArray = new byte[this.image.length];
        int i = 0;
        for (Byte wrappedByte : this.image){
            byteArray[i++] = wrappedByte; // auto unboxing
        }

        return byteArray;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }
}
