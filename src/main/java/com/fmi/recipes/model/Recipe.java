package com.fmi.recipes.model;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private boolean isDrink;

    @NotNull
    @Size(min = 4, max = 100)
    @Column(unique = true)
    private String name;

    @NotNull
    @NotBlank(message = "Description is required")
    @Column(length = 4096)
    private String description;

    @Lob
    private Byte[] image;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id", nullable = false)
    private User author;

    @NotNull
    private Date created = new Date();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "recipe")
    private Set<Ingredient> ingredients = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "recipe")
    private Set<ShoppingCartRecipe> shoppingCartRecipes;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "favorite_recipe",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> favorites = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean getIsDrink() {
        return isDrink;
    }

    public void setIsDrink(boolean drink) {
        isDrink = drink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Byte[] getRawImage() {
        return this.image;
    }

    public byte[] getImage() {
        byte[] byteArray = new byte[this.image.length];
        int i = 0;
        for (Byte wrappedByte : this.image){
            byteArray[i++] = wrappedByte; // auto unboxing
        }

        return byteArray;
    }

    public void setImage(MultipartFile file) {
        Byte[] byteObjects;
        try {
            byteObjects = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }
            this.image = byteObjects;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Set<User> getFavorites() {
        return favorites;
    }

    public void setFavorites(Set<User> favorites) {
        this.favorites = favorites;
    }

    @PreRemove
    private void removeShoppingCartRecipes() {
        for (ShoppingCartRecipe shoppingCartRecipe : this.shoppingCartRecipes) {
            shoppingCartRecipe.setRecipe(null);
        }
    }
}
