package com.fmi.recipes.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class ShoppingCartIngredient implements Comparable<ShoppingCartIngredient> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String quantity;

    @NotNull
    private boolean bought;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shopping_cart_recipe_id", nullable = false)
    private ShoppingCartRecipe shoppingCartRecipe;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public boolean isBought() {
        return bought;
    }

    public void setBought(boolean bought) {
        this.bought = bought;
    }

    public ShoppingCartRecipe getShoppingCartRecipe() {
        return shoppingCartRecipe;
    }

    public void setShoppingCartRecipe(ShoppingCartRecipe shoppingCartRecipe) {
        this.shoppingCartRecipe = shoppingCartRecipe;
    }

    @Override
    public int compareTo(ShoppingCartIngredient shoppingCartIngredient) {
        return this.getName().compareTo(shoppingCartIngredient.getName());
    }

    @Override
    public String toString() {
        return "ShoppingCartIngredient";
    }
}
