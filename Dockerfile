FROM maven:3.5.3-jdk-8-alpine as target

WORKDIR /build

COPY pom.xml .
RUN mvn dependency:go-offline

COPY src/ /build/src/
RUN mvn package

FROM openjdk:8-jre-alpine

COPY --from=target /build/target/recipes-0.0.1-SNAPSHOT.jar /app/recipes.jar

ENTRYPOINT ["java","-jar","/app/recipes.jar"]
